import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { DashComponent } from './dash.component';

describe('DashComponent', () => {
  let component: DashComponent;
  let fixture: ComponentFixture<DashComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`'should have as title 'dash'`, () => {
    fixture = TestBed.createComponent(DashComponent);
    component = fixture.debugElement.componentInstance;
    expect(component.title).toEqual('dash');
  });

  it(`should have a p tag of 'dash works!'`, () => {
    fixture = TestBed.createComponent(DashComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').innerText).toBe('dash works!!');
  });

  it('says hello', () => {
    fixture = TestBed.createComponent(DashComponent);
    component = fixture.componentInstance;
    expect(component.getDetails()).toEqual('Hello World!');
  });
});
