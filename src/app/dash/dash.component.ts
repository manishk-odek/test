import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit {
  title = 'dash';
  constructor() {}

  ngOnInit() {}

  getDetails() {
    return 'Hello World!';
  }
}
